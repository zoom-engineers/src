//Moving Average Header
#ifndef MAVG_H
#define MAVG_H

#include <stdint.h>

#define MAVG_LENGTH 4

typedef strcut {
        float buf[MAVG_LENGTH];
        unit8_t bufIndex;
        
        float out;
}MAVGFilter;

void MAVG_Init(MAVGFilter *fltr);
float MAVG_Update(MAVGFilter *fltr, float inp);

#endif